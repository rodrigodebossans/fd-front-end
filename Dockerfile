FROM node:12-slim

ARG USER_HOME_DIR="/tmp"
ARG USER_ID=1000

ENV HOME "$USER_HOME_DIR"

RUN apt-get update && apt-get install -qqy --no-install-recommends \
    build-essential \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN set -xe \
    && mkdir -p $USER_HOME_DIR \
    && chown $USER_ID $USER_HOME_DIR \
    && chmod a+rw $USER_HOME_DIR \
    && chown -R node /usr/local/lib /usr/local/include /usr/local/share /usr/local/bin \
    && (cd "$USER_HOME_DIR"; su node -c "npm install -g @angular/cli; npm cache clean --force")

EXPOSE 4200

USER root