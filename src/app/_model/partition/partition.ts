import { TreeNode } from 'primeng/api';
import { Directory } from '../directory/directory';

export class Partition {
  id?: number;
  name: string;
  directoryList?: Directory[] | TreeNode[];
  creationDate?: Date;
  modifiedDate?: Date;

  static fromObj(obj: any): Partition {
    if (obj) {
      const partition: Partition = new Partition();

      if (obj['id']) partition.id = obj['id'];
      partition.name = obj['name'];

      if (obj['directoryList']) partition.directoryList = obj['directoryList'];
      else partition.directoryList = [];

      if (obj['creationDate']) partition.creationDate = obj['creationDate'];
      if (obj['modifiedDate']) partition.modifiedDate = obj['modifiedDate'];

      return partition;
    }
  }

  static fromArray(array: any[]): Partition[] {
    return array.map((obj: any) => Partition.fromObj(obj));
  }
}
