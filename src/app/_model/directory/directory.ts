import { TreeNode } from 'primeng/api';
import { Partition } from './../partition/partition';

export class Directory {
  id?: number;
  name: string;
  parentDirectory?: Directory;
  childDirectoryList?: Directory[];
  partition: Partition;
  creationDate?: Date;
  modifiedDate?: Date;

  static fromObj(obj: any): Directory {
    if (obj) {
      const directory: Directory = new Directory();

      if (obj['id']) directory.id = obj['id'];

      directory.name = obj['name'];

      if (obj['parentDirectory'])
        directory.parentDirectory = Directory.fromObj(obj['parentDirectory']);

      if (obj['childDirectoryList'])
        directory.childDirectoryList = Directory.fromArray(
          obj['childDirectoryList']
        );

      directory.partition = Partition.fromObj(obj['partition']);

      if (obj['creationDate']) directory.creationDate = obj['creationDate'];

      if (obj['modifiedDate']) directory.modifiedDate = obj['modifiedDate'];

      return directory;
    }
  }

  static fromArray(array: any[]): Directory[] {
    return array.map((obj: any) => Directory.fromObj(obj));
  }

  static fromObjToTreeNode(obj: any): TreeNode {
    if (obj) {
      const treeNode: TreeNode = new Object();

      if (obj['id']) treeNode['id'] = obj['id'];

      if (obj['name']) {
        treeNode.label = obj['name'];
        treeNode.data = `${obj['name']} Folder`;
      }
      if (obj['icon']) treeNode.icon = obj['icon'];
      treeNode.expandedIcon = 'pi pi-folder-open';
      treeNode.collapsedIcon = 'pi pi-folder';

      if (obj['childDirectoryList'])
        treeNode.children = Directory.fromArrayToTreeNode(
          obj['childDirectoryList']
        );

      if (obj['leaf']) treeNode.leaf = obj['leaf'];
      if (obj['expanded']) treeNode.expanded = obj['expanded'];
      if (obj['type']) treeNode.type = obj['type'];
      if (obj['parentDirectory']) treeNode.parent = obj['parentDirectory'];
      if (obj['partialSelected'])
        treeNode.partialSelected = obj['partialSelected'];

      if (obj['styleClass']) treeNode.styleClass = obj['styleClass'];
      if (obj['droppable']) treeNode.droppable = obj['droppable'];
      if (obj['selectable']) treeNode.selectable = obj['selectable'];
      if (obj['key']) treeNode.key = obj['key'];

      return treeNode;
    }
  }

  static fromArrayToTreeNode(array: any[]): TreeNode[] {
    return array.map((obj: any) => Directory.fromObjToTreeNode(obj));
  }

  static fromTreeNodeToObj(treeNode: any): Directory {
    if (treeNode) {
      const directory: Directory = new Directory();

      if (treeNode['id']) directory.id = treeNode['id'];

      if (treeNode['label']) directory.name = treeNode['label'];

      if (treeNode['children'])
        directory.childDirectoryList = Directory.fromArrayTreeNodeToObj(
          treeNode['children']
        );

      return directory;
    }
  }

  static fromArrayTreeNodeToObj(array: any[]): Directory[] {
    return array.map((obj: any) => Directory.fromTreeNodeToObj(obj));
  }
}
