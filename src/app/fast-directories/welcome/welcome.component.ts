import { Partition } from './../../_model/partition/partition';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { PartitionService } from 'src/app/_service/partition/partition.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
})
export class WelcomeComponent implements OnInit {
  public visibleDialog: boolean = false;

  constructor(
    private messageService: MessageService,
    private partitionService: PartitionService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  public onClickNewPartition(): void {
    this.visibleDialog = true;
  }

  public onSubmittedPartitionForm({ partition }): void {
    if (partition) {
      this.partitionService.create(partition).subscribe(
        (partition: Partition) => {
          if (partition && partition.id) {
            this.messageService.add({
              severity: 'success',
              summary: 'Tudo certo!',
              detail: 'Partição cadastrada com sucesso.',
            });
            this.router.navigate(['/partition']);
          }
        },
        (error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Erro!',
            detail: 'Parece que algo deu errado.',
          });
        }
      );
    }
    this.visibleDialog = false;
  }
}
