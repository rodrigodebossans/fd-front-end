import { WelcomeGuard } from './../_guard/welcome.guard';
import { FastDirectoriesComponent } from './fast-directories.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'welcome' },
  {
    path: 'welcome',
    loadChildren: () =>
      import('./welcome/welcome.module').then((m) => m.WelcomeModule),
  },
  {
    path: 'partition',
    loadChildren: () =>
      import('./partition/partition.module').then((m) => m.PartitionModule),
    canActivate: [WelcomeGuard],
  },
  { path: '**', pathMatch: 'full', redirectTo: 'welcome' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FastDirectoriesRoutingModule {}
