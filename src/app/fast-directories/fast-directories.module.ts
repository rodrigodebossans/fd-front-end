import { FastDirectoriesComponent } from './fast-directories.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FastDirectoriesRoutingModule } from './fast-directories-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { TreeModule } from 'primeng/tree';
import { SidebarModule } from 'primeng/sidebar';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ButtonModule } from 'primeng/button';
import { WelcomeGuard } from '../_guard/welcome.guard';

@NgModule({
  declarations: [FastDirectoriesComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FastDirectoriesRoutingModule,
    HttpClientModule,
    SidebarModule,
    TreeModule,
    ContextMenuModule,
    DynamicDialogModule,
    ButtonModule,
  ],
  providers: [WelcomeGuard],
})
export class FastDirectoriesModule {}
