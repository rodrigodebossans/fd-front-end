import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FastDirectoriesComponent } from './fast-directories.component';

describe('FastDirectoriesComponent', () => {
  let component: FastDirectoriesComponent;
  let fixture: ComponentFixture<FastDirectoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FastDirectoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FastDirectoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
