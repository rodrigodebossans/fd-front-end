import { NgxSpinnerService } from 'ngx-spinner';
import { PartitionService } from 'src/app/_service/partition/partition.service';
import { Partition } from './../../_model/partition/partition';
import { catchError, map } from 'rxjs/operators';
import { DirectoryService } from './../../_service/directory/directory.service';
import { Directory } from './../../_model/directory/directory';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {
  MenuItem,
  TreeNode,
  MessageService,
  ConfirmationService,
} from 'primeng/api';
import { Observable, Subject, empty } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css'],
})
export class DirectoryComponent implements OnInit {
  public visibleDialog: boolean = false;
  public operationForm: string = null;

  public itemsContextMenu: MenuItem[];
  public selectedDirectoryTreeNode: TreeNode;

  public selectedDirectory: Directory = new Directory();

  public actualPartition$: Observable<Partition>;
  public actualPartition: Partition = new Partition();

  public nameActualPartitionSubject$: Subject<string> = new Subject<string>();
  public errorDirectories$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private partitionService: PartitionService,
    private directoryService: DirectoryService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private route: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.initContextMenus();
    this.loadActualPartition();
    this.spinner.show();
  }

  private getActualPartitionId(): number {
    return parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
  }

  private initContextMenus(): void {
    this.itemsContextMenu = [
      {
        label: 'Novo diretório',
        icon: 'pi pi-plus',
        command: (event) => this.newChildDirectory(),
      },
      {
        label: 'Editar',
        icon: 'pi pi-pencil',
        command: (event) => this.editDirectory(),
      },
      {
        label: 'Excluir',
        icon: 'pi pi-trash',
        command: (event) => this.deleteDirectory(),
      },
      {
        label: 'Deselecionar',
        icon: 'pi pi-times',
        command: (event) => this.unselectDirectory(),
      },
    ];
  }

  private loadActualPartition(): void {
    this.actualPartition$ = this.partitionService
      .readByIdWithDirectoryTree(this.getActualPartitionId())
      .pipe(
        map(
          (partition) => (
            (partition.directoryList = Directory.fromArrayToTreeNode(
              partition.directoryList
            )),
            this.nameActualPartitionSubject$.next(partition.name),
            (this.actualPartition = partition),
            partition
          )
        ),
        catchError((err) => {
          if (err?.error?.error?.name === 'NonExistentObjectException')
            this.route.navigate(['/']);

          if (err) this.errorDirectories$.next(true);
          return empty();
        })
      );
  }

  private setSelectedDirectory(directory: Directory): void {
    this.selectedDirectory = Directory.fromObj(directory);
    this.selectedDirectory.partition = {
      ...this.actualPartition,
      directoryList: Directory.fromArrayTreeNodeToObj(
        this.actualPartition.directoryList
      ),
    };
  }

  public onClickNewDirectory() {
    this.operationForm = 'create';
    this.setSelectedDirectory(new Directory());
    this.visibleDialog = true;
  }

  public onSubmittedDirectoryForm({ directory, operation }): void {
    if (directory && operation) {
      switch (operation) {
        case 'create':
          this.directoryService.create(directory).subscribe(
            (directory: Directory) => {
              if (directory?.id) {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Tudo certo!',
                  detail: 'Diretório cadastrado com sucesso.',
                });
                this.loadActualPartition();
              }
            },
            (httpErrorResponse: HttpErrorResponse) => {
              this.messageService.add({
                severity: 'error',
                summary: 'Erro!',
                detail: 'Parece que algo deu errado.',
              });
            }
          );
          break;

        case 'edit':
          this.directoryService.update(directory.id, directory).subscribe(
            (directory: Directory) => {
              if (directory?.id) {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Tudo certo!',
                  detail: 'Diretório editado com sucesso.',
                });
                this.loadActualPartition();
              }
            },
            (httpErrorResponse: HttpErrorResponse) => {
              this.messageService.add({
                severity: 'error',
                summary: 'Erro!',
                detail: 'Parece que algo deu errado.',
              });
            }
          );
          break;

        default:
          break;
      }
    }
    this.visibleDialog = false;
  }

  public newChildDirectory(): void {
    const directory = new Directory();
    directory.parentDirectory = Directory.fromTreeNodeToObj(
      this.selectedDirectoryTreeNode
    );
    this.setSelectedDirectory(directory);
    this.operationForm = 'create';
    this.visibleDialog = true;
  }

  private getDirectoryFromSelectedDirectoryTreeNode(): Directory {
    return Directory.fromTreeNodeToObj(this.selectedDirectoryTreeNode);
  }

  private editDirectory(): void {
    this.setSelectedDirectory(this.getDirectoryFromSelectedDirectoryTreeNode());
    this.operationForm = 'edit';
    this.visibleDialog = true;
  }

  private deleteDirectory(): void {
    this.setSelectedDirectory(this.getDirectoryFromSelectedDirectoryTreeNode());

    const { id } = this.selectedDirectory;
    const { length } = this.selectedDirectory.childDirectoryList;
    const { name } = this.selectedDirectory;

    this.confirmationService.confirm({
      header: `Deletar diretório ${name}`,
      icon: 'pi pi-exclamation-circle',
      message:
        length > 0
          ? `Existem ${length} diretórios neste diretório, deseja realmente deletá-lo?`
          : `Deseja realmente deletar este diretório?`,
      acceptLabel: 'Confirmar',
      rejectLabel: 'Cancelar',
      accept: () => {
        this.directoryService.delete(id).subscribe(
          (directory: Directory) => {
            if (directory?.id) {
              this.messageService.add({
                severity: 'success',
                summary: 'Tudo certo!',
                detail: 'Diretório deletado com sucesso.',
              });
              this.loadActualPartition();
            }
          },
          (httpErrorResponse: HttpErrorResponse) => {
            this.messageService.add({
              severity: 'error',
              summary: 'Erro!',
              detail: 'Parece que algo deu errado.',
            });
          }
        );
      },
    });
  }

  private unselectDirectory() {
    this.selectedDirectoryTreeNode = null;
  }
}
