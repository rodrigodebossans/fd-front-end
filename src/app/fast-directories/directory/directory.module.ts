import { HttpClientModule } from '@angular/common/http';
import { MenuModule } from 'primeng/menu';
import { DirectoryComponent } from './directory.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectoryRoutingModule } from './directory-routing.module';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { TreeModule } from 'primeng/tree';
import { ContextMenuModule } from 'primeng/contextmenu';

import { NgxSpinnerModule } from "ngx-spinner";

import { FastDirectoriesFormsModule } from '../_form/fast-directories-forms.module';


@NgModule({
  declarations: [DirectoryComponent],
  imports: [
    CommonModule,
    DirectoryRoutingModule,
    HttpClientModule,
    ButtonModule,
    DialogModule,
    MenuModule,
    TreeModule,
    ContextMenuModule,
    FastDirectoriesFormsModule,
    NgxSpinnerModule,
  ]
})
export class DirectoryModule { }
