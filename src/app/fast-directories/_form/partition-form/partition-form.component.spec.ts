import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartitionFormComponent } from './partition-form.component';

describe('PartitionFormComponent', () => {
  let component: PartitionFormComponent;
  let fixture: ComponentFixture<PartitionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartitionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartitionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
