import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Partition } from '../../../_model/partition/partition';

@Component({
  selector: 'app-partition-form',
  templateUrl: './partition-form.component.html',
  styleUrls: ['./partition-form.component.css'],
})
export class PartitionFormComponent implements OnInit {
  @Input() operation: string = 'create';
  @Input() partition: Partition = new Partition();

  @Output() submitted = new EventEmitter<Object>();

  constructor() {}

  ngOnInit(): void {}

  onSubmit(partitionForm): void {
    if (this.partition)
      this.submitted.emit({
        partition: this.partition,
        operation: this.operation,
      });
  }
}
