import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Directory } from 'src/app/_model/directory/directory';

@Component({
  selector: 'app-directory-form',
  templateUrl: './directory-form.component.html',
  styleUrls: ['./directory-form.component.css'],
})
export class DirectoryFormComponent implements OnInit {
  @Input() operation: string = 'create';
  @Input() directory: Directory = new Directory();

  @Output() submitted = new EventEmitter<Object>();

  constructor() {}

  ngOnInit(): void {}

  onSubmit(directoryForm): void {
    if (this.directory)
      this.submitted.emit({
        directory: this.directory,
        operation: this.operation,
      });
  }
}
