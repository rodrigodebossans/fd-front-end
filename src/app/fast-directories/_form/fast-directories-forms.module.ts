import { FormsModule } from '@angular/forms';
import { DirectoryFormComponent } from './directory-form/directory-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartitionFormComponent } from './partition-form/partition-form.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  declarations: [PartitionFormComponent, DirectoryFormComponent],
  imports: [CommonModule, FormsModule, ButtonModule, InputTextModule],
  exports: [PartitionFormComponent, DirectoryFormComponent],
})
export class FastDirectoriesFormsModule {}
