import { PartitionComponent } from './partition.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: PartitionComponent },
  {
    path: ':id/directories',
    loadChildren: () =>
      import('../directory/directory.module').then((m) => m.DirectoryModule),
  },
  { path: '**', pathMatch: 'full', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PartitionRoutingModule {}
