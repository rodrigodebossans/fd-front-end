import { Component, OnInit } from '@angular/core';
import { Observable, Subject, empty } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ConfirmationService } from 'primeng/api';
import { PartitionService } from './../../_service/partition/partition.service';
import { MenuItem, MessageService } from 'primeng/api';
import { Partition } from 'src/app/_model/partition/partition';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-partition',
  templateUrl: './partition.component.html',
  styleUrls: ['./partition.component.css'],
})
export class PartitionComponent implements OnInit {
  public visibleDialog: boolean = false;
  public operationForm: string = null;

  public itemsContextMenu: MenuItem[];

  selectedPartition: Partition = new Partition();

  partitions$: Observable<Partition[]> = null;
  error$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private partitionService: PartitionService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.initContextMenus();
    this.loadPartitions();
    this.spinner.show();
  }

  initContextMenus(): void {
    this.itemsContextMenu = [
      {
        label: 'Opções',
        items: [
          {
            label: 'Editar',
            icon: 'pi pi-pencil',
            command: () => {
              this.updatePartition();
            },
          },
          {
            label: 'Deletar',
            icon: 'pi pi-trash',
            command: () => {
              this.deletePartition();
            },
          },
        ],
      },
    ];
  }

  loadPartitions(): void {
    this.partitions$ = this.partitionService.read().pipe(
      catchError((err) => {
        this.error$.next(true);
        return empty();
      })
    );
  }

  setSelectedPartition(partition: Partition): void {
    this.selectedPartition = Partition.fromObj(partition);
  }

  onClickNewPartition(): void {
    this.operationForm = 'create';
    this.selectedPartition = new Partition();
    this.visibleDialog = true;
  }

  onSubmittedPartitionForm({ partition, operation }): void {
    if (partition && operation) {
      switch (operation) {
        case 'create':
          this.partitionService.create(partition).subscribe(
            (partition: Partition) => {
              if (partition?.id) {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Tudo certo!',
                  detail: 'Partição cadastrada com sucesso.',
                });
                this.loadPartitions();
              }
            },
            (httpErrorResponse: HttpErrorResponse) => {
              this.messageService.add({
                severity: 'error',
                summary: 'Erro!',
                detail: 'Parece que algo deu errado.',
              });
            }
          );
          break;

        case 'edit':
          this.partitionService.update(partition.id, partition).subscribe(
            (partition: Partition) => {
              if (partition?.id) {
                this.messageService.add({
                  severity: 'success',
                  summary: 'Tudo certo!',
                  detail: 'Partição editada com sucesso.',
                });
                this.loadPartitions();
              }
            },
            (httpErrorResponse: HttpErrorResponse) => {
              this.messageService.add({
                severity: 'error',
                summary: 'Erro!',
                detail: 'Parece que algo deu errado.',
              });
            }
          );
          break;

        default:
          break;
      }
    }
    this.visibleDialog = false;
  }

  updatePartition(): void {
    this.operationForm = 'edit';
    this.visibleDialog = true;
  }

  deletePartition(): void {
    const { id } = this.selectedPartition;
    const { length } = this.selectedPartition.directoryList;
    const { name } = this.selectedPartition;

    this.confirmationService.confirm({
      header: `Deletar partição ${name}`,
      icon: 'pi pi-exclamation-circle',
      message:
        length > 0
          ? `Existem ${length} diretórios nesta partição, deseja realmente deletá-la?`
          : `Deseja realmente deletar esta partição?`,
      acceptLabel: 'Confirmar',
      rejectLabel: 'Cancelar',
      accept: () => {
        this.partitionService.delete(id).subscribe(
          (partition: Partition) => {
            if (partition?.id) {
              this.messageService.add({
                severity: 'success',
                summary: 'Tudo certo!',
                detail: 'Partição deletada com sucesso.',
              });
              this.loadPartitions();
            }
          },
          (httpErrorResponse: HttpErrorResponse) => {
            this.messageService.add({
              severity: 'error',
              summary: 'Erro!',
              detail: 'Parece que algo deu errado.',
            });
          }
        );
      },
    });
  }

  toggleMenu(menu, event, partition: Partition): void {
    this.setSelectedPartition(partition);
    menu.toggle(event);
  }
}
