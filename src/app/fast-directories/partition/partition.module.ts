import { DirectoryModule } from './../directory/directory.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartitionRoutingModule } from './partition-routing.module';
import { PartitionComponent } from './partition.component';
import { ButtonModule } from 'primeng/button';
import { MenuModule } from 'primeng/menu';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';

import { NgxSpinnerModule } from "ngx-spinner";

import { FastDirectoriesFormsModule } from '../_form/fast-directories-forms.module';


@NgModule({
  declarations: [PartitionComponent],
  imports: [
    CommonModule,
    PartitionRoutingModule,
    ButtonModule,
    MenuModule,
    DialogModule,
    InputTextModule,
    FastDirectoriesFormsModule,
    DirectoryModule,
    NgxSpinnerModule,
  ],
})
export class PartitionModule {}
