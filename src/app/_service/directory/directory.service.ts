import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Directory } from './../../_model/directory/directory';
import { environment } from './../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class DirectoryService {
  coreUrl: string = environment.coreUrl + 'directory';

  constructor(private http: HttpClient) {}

  create(directory: Directory): Observable<Directory> {
    return this.http.post<Directory>(this.coreUrl, directory);
  }

  read(): Observable<Directory[]> {
    return this.http.get<Directory[]>(this.coreUrl);
  }

  update(id: number, directory: Directory): Observable<Directory> {
    return this.http.put<Directory>(this.coreUrl + `/${id}`, directory);
  }

  delete(id: number): Observable<Directory> {
    return this.http.delete<Directory>(this.coreUrl + `/${id}`);
  }
}
