import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Partition } from './../../_model/partition/partition';
import { environment } from './../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class PartitionService {
  coreUrl: string = environment.coreUrl + 'partition';

  constructor(private http: HttpClient) {}

  public create(partition: Partition): Observable<Partition> {
    return this.http.post<Partition>(this.coreUrl, partition);
  }

  public read(): Observable<Partition[]> {
    return this.http.get<Partition[]>(this.coreUrl);
  }

  public readByIdWithDirectoryTree(id: number): Observable<Partition> {
    return this.http.get<Partition>(
      this.coreUrl + `/read-by-id-with-directory-tree/${id}`
    );
  }

  public update(id: number, partition: Partition): Observable<Partition> {
    return this.http.put<Partition>(this.coreUrl + `/${id}`, partition);
  }

  public delete(id: number): Observable<Partition> {
    return this.http.delete<Partition>(this.coreUrl + `/${id}`);
  }
}
