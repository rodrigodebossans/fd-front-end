import { PartitionService } from './../_service/partition/partition.service';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Partition } from '../_model/partition/partition';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WelcomeGuard implements CanActivate {
  constructor(
    private partitionService: PartitionService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.partitionService.read().pipe(
      map((partitions: Partition[]) => {
        if (partitions && partitions.length) return true;
        else {
          this.router.navigate(['/welcome']);
          return false;
        }
      })
    );
  }
}
